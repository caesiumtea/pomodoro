# Pomodoro timer

Simple web app to run a task timer for the [Pomodoro method](https://en.wikipedia.org/wiki/Pomodoro_Technique). Based on a [tutorial from codedex.io](https://www.codedex.io/projects/build-a-pomodoro-app-with-html-css-js), by [Dana Lee](https://twitter.com/danamitecoder). Most code here is just copied from the tutorial and is not my own.

[View the app live on GitLab Pages](https://caesiumtea.gitlab.io/pomodoro/)

All feedback is welcome!

## My changes
- Root font size is specified in percent instead of px to respect users' font size defaults ([read more about font size accessibility](https://css-tricks.com/accessible-font-sizing-explained/))
- Margins updated to be in rem instead of px, so they also adjust relative to user font size
  - However, I didn't remove *all* pixel sizing! It made sense to keep the timer graphic sized in pixels, so I also left the container and spacing around that element sized in pixels to match.
- Top margin removed from some elements because it was getting eaten by margin collapse anyway
- `.app-container` uses `width: fit-content; min-width: 250px;` instead of setting a constant width in px, in order to accommodate the possibility of larger text inside if the user's default font size is larger
- Added Droid Sans Mono to the Google Fonts import, because it was actually not listed in there yet
- Removed a few lines of CSS that didn't seem to change anything:
  - Didn't set `color` on `h1` because it already inherits that color from root
  - Didn't set `background-size: cover;` because it didn't appear to change anything and I think it's the default behavior of the linear gradient function
- Renamed the JS variable `state` to `readyToStart` to be more descriptive

## Further development
Some features I'd like to try adding (once I learn more JavaScript, for most of them):
- Make the start button selectable by keyboard instead of just by clicking
- After the timer ends, automatically reset it, so you don't have to refresh the page to use it again
- Start button changes to a reset button once the countdown has started
- Pause button?
- Track how many sessions have been done in a row
- Let the user change the session length
- Change the colors as a warning sign when time is getting low
- Animate the circle around the timer to show what fraction of the time is left

## License
Copyright Dana Lee and Codédex
  
## Acknowledgements
- Aside from my modifications, code originally written by [Dana Lee](https://twitter.com/danamitecoder)
- Bell sound: "Attention bell ding" from [Mixkit](https://mixkit.co/free-sound-effects/bell/)

## Author
Hey, I'm **caesiumtea**, AKA Vance! Feel free to contact me with any feedback.
- [Website and social links](https://caesiumtea.glitch.me/)
- [@caesiumtea_dev on Twitter](https://www.twitter.com/caesiumtea_dev)
- [@entropy@mastodon.social](https://mastodon.social/@entropy)
